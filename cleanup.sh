#!/bin/bash -x

oc project default # for convenience, not generally portable but fine with default system:admin setup on `cluster up`. 
oc delete project cicd-tasks-dev
oc delete project cicd-tasks-prod
oc delete project cicd-components
oc delete project cicd-jenkins

oc delete clusterrolebinding jenkins-cluster-admin
