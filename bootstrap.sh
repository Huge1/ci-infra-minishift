#!/bin/bash -x

#You might consider cleanup, but it made my process too wordy, when there is noting to clean

if [ "$1" != "" ]; then
    DNS_ROOT_PARAM_OR_EMPTY=" -p JENKIS_DOMAIN=$1"
else
    DNS_ROOT_PARAM_OR_EMPTY=""
fi
oc process -f templates/jenkins-project-template.yaml | oc create -f -
oc project cicd-jenkins
# We need to pull the desired template to *that* project, otherwise it's not found, or we'd have to specify namespace, likely.
#oc create -f https://raw.githubusercontent.com/openshift/origin/master/examples/jenkins/jenkins-persistent-template.json
oc create -f templates/jenkins-persistent-template.json 
  # ^prepares agent images and also creates route for jenkins, although expecting conflict is not the cleanest way I'd like./
    #@toDo is maybe take more responsibility for the jenkins-persistent template.
  
#oc new-app --template=jenkins-persistent --param VOLUME_CAPACITY=2Gi MEMORY_LIMIT=1200Mi # Not enough remaining on my HD, this seems enough for Jenkins.)
# For now, I'd rather recommned allocating larger volume as it the pod seemed using RWO storage type that is something to debug @todo
oc new-app --template=jenkins-persistent $DNS_ROOT_PARAM_OR_EMPTY --param VOLUME_CAPACITY=6Gi MEMORY_LIMIT=1200Mi 

oc process -f templates/jenkins-infra-pipeline-template.yaml | oc create -f -
#oc process -f templates/jenkins-app-pipeline-template.yaml | oc create -f - # In theory we can move creation of app pipeline to a step of infra pipeline.

oc start-build bc/infra-pipeline -n cicd-jenkins
